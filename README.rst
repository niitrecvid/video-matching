TRECVID 2016: Video and Text matching
=====================================

Problem setup
=====================

Assume that we have :math:`N` videos :math:`V_1, V_2, \ldots ,V_N` and :math:`N` captions  :math:`C_1, C_2, \ldots ,C_N`, but we do not know which caption for which video. Our task is matching videos and captions, i.e., for which video :math:`V_i`, we have to rank the captions in a ranking list :math:`C_{i,1},C_{i,2},\ldots ,C_{i,N}` in semantic order to the content of :math:`V_i`.

Methods
===============

Method 1: 2D-VGG + Lucene score
---------------------------------------------

- First, we use a 2D-VGG (16 layers CNN which is pretrained on `ImageNet <http://image-net.org>`_) to extract several concepts from each frames of a video :math:`V`. For example, from a video we have concepts :math:`c_1,c_2,\ldots ,c_n`.

