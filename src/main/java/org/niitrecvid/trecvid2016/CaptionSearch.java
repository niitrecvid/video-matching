package org.niitrecvid.trecvid2016;

import lombok.NonNull;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.niitrecvid.util.Evaluation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/23.
 */
public class CaptionSearch {
    private static final Logger log  = LoggerFactory.getLogger(CaptionSearch.class);
    protected SimpleMatcher matcher;
    protected List<String> goodConcepts;
    protected int[] rankedList;

    public CaptionSearch(@NonNull SimpleMatcher matcher) throws IOException {
        this.matcher = matcher;
    }


    public List<String> getGoodConcepts() {
        return this.goodConcepts;
    }

    public int[] getRankedList() {
        return this.rankedList;
    }

    public void search(@NonNull String conceptFile) throws IOException, ParseException {
        Set<String> concepts = SimpleMatcher.loadConcepts(conceptFile, 0);
        goodConcepts = new ArrayList<>();
        if (matcher == null) {
            throw new IllegalStateException("Matcher must be initialized before being passed here");
        }
        IndexReader indexReader = DirectoryReader.open(matcher.getIndex());
        IndexSearcher searcher = new IndexSearcher(indexReader);
        final double[] captionScores = new double[matcher.numDocs()];
        Integer[] idx = new Integer[captionScores.length];
        for (int i = 0; i < captionScores.length; i++) {
            captionScores[i] = 0.0f;
            idx[i] = i;
        }
        for (String concept:concepts) {
            ScoreDoc[] docs = matcher.query(concept, 10);
            if (docs.length > 0) {
                goodConcepts.add(concept);
                for (int i = 0; i < docs.length; i++) {
                    Document d = searcher.doc(docs[i].doc);
                    captionScores[docs[i].doc] += docs[i].score;
                }
            }
        }

        log.info("Good concepts for this video: " + goodConcepts.toString());

        Arrays.sort(idx, new Comparator<Integer>() {
            @Override
            public int compare(final Integer o1, final Integer o2) {
                return Double.compare(captionScores[o2], captionScores[o1]);
            }
        });
        rankedList =  new int[idx.length];
        for (int i = 0; i < idx.length; i++) {
            rankedList[i] = idx[i];
        }
    }

    public double invertedRank(int groundTruth) {
        return new Evaluation().invertedRank(rankedList, groundTruth);
    }

    public double precision(int groundTruth, int k) {
        return new Evaluation().precision(rankedList, groundTruth, k);
    }
}
