package org.niitrecvid.trecvid2016;

import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.EmbeddingLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A model to implement fastText: https://github.com/facebookresearch/fastText
 *
 * Reference:
 * [1] P. Bojanowski, E. Grave, A. Joulin, T. Mikolov,
 * Enriching Word Vectors with Subword Information, arXiv preprint arXiv:1607.04606, 2016
 * [2] A. Joulin, E. Grave, P. Bojanowski, T. Mikolov,
 * Bag of Tricks for Efficient Text Classification, arXiv preprint arXiv:1607.01759, 2016
 *
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/29.
 */
public class FastText {
    private static final Logger log = LoggerFactory.getLogger(FastText.class);
    protected int inputDim, embedDim, numInput;
    protected MultiLayerNetwork network;
    protected Evaluation eval;

    public FastText(int inputDim, int embedDim, int numInput) {
        this.inputDim = inputDim;
        this.embedDim = embedDim;
        this.numInput = numInput;
    }

    public void init() {
        MultiLayerConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
                .seed(12345)
                .iterations(1).regularization(true).l2(0.0005)
                .learningRate(0.001)
                .weightInit(WeightInit.XAVIER)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(Updater.NESTEROVS).momentum(0.9)
                .list()
                .layer(0, new EmbeddingLayer.Builder()
                        .nIn(inputDim)
                        .nOut(embedDim)
                        .name("embed1")
                        .build())
                .layer(1, new SubsamplingLayer.Builder()
                        .poolingType(SubsamplingLayer.PoolingType.AVG)
                        .name("meanpool1")
                        .build())
                .layer(2, new DenseLayer.Builder()
                        .nOut(1)
                        .activation("sigmoid")
                        .name("dense1")
                        .build())
                .layer(2, new OutputLayer.Builder()
                        .nOut(1)
                        .lossFunction(LossFunctions.LossFunction.RECONSTRUCTION_CROSSENTROPY)
                        .name("output")
                        .build())
                .setInputType(InputType.recurrent(numInput));

        MultiLayerConfiguration configuration = builder.build();
        network = new MultiLayerNetwork(configuration);
        network.init();
        network.setListeners(new ScoreIterationListener(1));
    }

    public void train(DataSetIterator dataSetIterator, int nEpochs) {
        for (int i = 0; i < nEpochs; i++) {
            network.fit(dataSetIterator);
        }
    }

     public void evaluate(DataSetIterator dataSetIterator) {
         while (dataSetIterator.hasNext()) {
             DataSet ds = dataSetIterator.next();

         }
     }
}
