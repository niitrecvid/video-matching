package org.niitrecvid.trecvid2016;

import lombok.NonNull;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.AbstractCache;
import org.deeplearning4j.text.documentiterator.LabelsSource;
import org.deeplearning4j.text.sentenceiterator.BasicLineIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/23.
 */
public class SentenceVector {
    private static final Logger log = LoggerFactory.getLogger(SentenceVector.class);
    SentenceIterator sentenceIterator;
    TokenizerFactory tokenizerFactory;
    LabelsSource labelsSource;
    ParagraphVectors paragraphVectors;
    AbstractCache<VocabWord> cache;

    public SentenceVector(@NonNull String filename, int nEpochs, double learningRate) throws IOException {
        File sentenceFile = new File(filename);
        sentenceIterator = new BasicLineIterator(sentenceFile);
        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
        labelsSource = new LabelsSource("SEN_");
        cache = new AbstractCache<>();
        paragraphVectors = new ParagraphVectors.Builder()
                .minWordFrequency(1)
                .iterations(5)
                .epochs(nEpochs)
                .layerSize(300)
                .learningRate(learningRate)
                .labelsSource(labelsSource)
                .windowSize(5)
                .iterate(sentenceIterator)
                .trainWordVectors(true) // train Word embedding too
                .vocabCache(cache)
                .tokenizerFactory(tokenizerFactory)
                .sampling(0)
                .build();
    }

    public void train() {
        paragraphVectors.fit();
    }

    public INDArray vector(@NonNull String str) {
        return paragraphVectors.lookupTable().vector(str);
    }
}
