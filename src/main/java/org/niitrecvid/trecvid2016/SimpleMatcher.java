package org.niitrecvid.trecvid2016;

import lombok.NonNull;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.deeplearning4j.text.tokenization.tokenizer.DefaultTokenizer;
import org.deeplearning4j.text.tokenization.tokenizer.Tokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * A matcher to matching concepts with captions for TRECVID Video-To-Text 2016 challenge
 *
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/22.
 */
public class SimpleMatcher {
    private final static Logger log = LoggerFactory.getLogger(SimpleMatcher.class);
    protected StandardAnalyzer analyzer;
    protected Directory index;
    protected IndexWriterConfig writerConfig;
    protected IndexWriter indexWriter;
    protected IndexSearcher indexSearcher;

    public SimpleMatcher() throws IOException {
        analyzer = new StandardAnalyzer();
        index = new RAMDirectory();
        writerConfig = new IndexWriterConfig(analyzer);
        indexWriter = new IndexWriter(index, writerConfig);
    }

    /**
     * Load all concepts
     *
     * @param conceptFile
     * @param skipLines
     * @return
     * @throws IOException
     */
    public static Set<String> loadConcepts(@NonNull String conceptFile, int skipLines) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(conceptFile));
        String line;
        Set<String> tokens = new HashSet<>();
        int count = 0;
        while((line = bufferedReader.readLine()) != null) {
            if (count >= skipLines) {
                line = line.replaceAll("[^a-zA-Z ]", "").toLowerCase();
                Tokenizer tokenizer = new DefaultTokenizer(line);
                tokens.addAll(tokenizer.getTokens());
            }
            count++;
        }

        return tokens;
    }

    /**
     * Add a caption
     *
     * @param caption
     * @throws IOException
     */
    protected void add(@NonNull String caption) throws IOException {
        Document doc = new Document();
        doc.add(new TextField("caption", caption, Field.Store.YES));
        indexWriter.addDocument(doc);
    }

    /**
     * Load all captions into RAM indexer
     *
     * @param filename
     * @throws IOException
     */
    public void loadCaptions(@NonNull String filename) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
        String line;
        while((line = bufferedReader.readLine()) != null) {
            line = line.substring(3).trim();
            this.add(line);
        }
        indexWriter.commit();
    }

    /**
     * Query for a concept
     *
     * @param query
     * @param hitsPerPage
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public ScoreDoc[] query(@NonNull String query, int hitsPerPage) throws IOException, ParseException {
        Query q = new QueryParser("caption",analyzer).parse(query);
        IndexReader indexReader = DirectoryReader.open(index);
        indexSearcher = new IndexSearcher(indexReader);
        TopDocs topDocs = indexSearcher.search(q, hitsPerPage);
        indexReader.close();
        return topDocs.scoreDocs;
    }

    /**
     * Finishing
     *
     * @throws IOException
     */
    public void finish() throws IOException {
        indexWriter.close();
    }

    public Directory getIndex() {
        return index;
    }

    public int numDocs() throws IOException {
        return DirectoryReader.open(index).numDocs();
    }
}
