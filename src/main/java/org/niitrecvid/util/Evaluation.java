package org.niitrecvid.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * An evaluation class for matching task
 *
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/23.
 */
public class Evaluation {
    private final static Logger log = LoggerFactory.getLogger(Evaluation.class);

    public Evaluation() {

    }

    public double precision(int[] rankedList, int groundTruth, int k) {
        int[] subRankedList = Arrays.copyOfRange(rankedList, 0, k);
        int pos = Arrays.binarySearch(subRankedList, groundTruth) + 1;
        if (pos > 0) return 1.0;
        else return 0;
    }

    public double precision(int[][] rankedLists, int[] groundTruths, int k) {
        double p = 0;
        for (int i = 0; i < rankedLists.length; i++) {
            p += precision(rankedLists[i], groundTruths[i], k);
        }

        return p / rankedLists.length;
    }

    public double invertedRank(int[] rankedList, int groundTruth) {
        int pos = Arrays.binarySearch(rankedList, groundTruth) + 1;
        if (pos == 0) {
            return 0;
        } else return 1.0 / pos;
    }

    public double meanInvertedRank(int[][] rankedLists, int[] groundTruths) {
        if (rankedLists.length != groundTruths.length || groundTruths.length == 0) {
            throw new IllegalStateException("Input must has the same sizes and must have at least 1 element");
        }

        double mir = 0.0;
        int n = groundTruths.length;
        for (int i = 0; i < n; i++) {
            mir += invertedRank(rankedLists[i], groundTruths[i]);
        }

        return mir / n;
    }


}
