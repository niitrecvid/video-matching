package org.niitrecvid.util;

import org.deeplearning4j.datasets.fetchers.BaseDataFetcher;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;

import java.io.IOException;
import java.net.URI;
import java.util.List;

/**
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/26.
 */
public class VideoDataFetcher extends BaseDataFetcher {
    protected URI[] locations;
    protected List<String> captions;
    protected VideoPreprocessor preprocessor;
    protected int width, height, n;

    public VideoDataFetcher(URI[] locations, List<String> captions, int width, int height, int n) {
        this.locations = locations;
        this.captions = captions;
        if (locations.length != captions.size()) throw new AssertionError();
        preprocessor = new VideoPreprocessor(width, height, n);
        this.width = width;
        this.height = height;
        this.n = n;
        this.cursor = 0;
    }

    @Override
    public void fetch(int batchSize) {
        int realSize = (cursor + batchSize >= locations.length) ? (locations.length - cursor): batchSize;
        URI[] ls = new URI[batchSize];
        for (int i = cursor; i < cursor + realSize; i++) {
            ls[i-cursor] = locations[i];
        }

        INDArray data = Nd4j.create(realSize, n, 3, width, height);
        INDArray caps = Nd4j.create(realSize, 1);
        for (int i = 0; i < realSize; i++) {
            try {
                INDArray v = preprocessor.loadVideo(ls[i].getRawPath());
                log.info("Processing " + ls[i]);
                data.putRow(i, v);
                data.reshape('f', realSize * n, 3, width, height); // reshape to fit Convolution2D layer
                caps.put(i, Nd4j.create(new float[]{i+cursor}));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        curr = new DataSet(data, caps);

        cursor += batchSize;
        if (cursor >= locations.length)
            cursor = 0;
    }

    public String getCaption(int i) {
        int id = curr.getLabels().getInt(i);
        return captions.get(id);
    }
}
