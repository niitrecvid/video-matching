package org.niitrecvid.util;

import org.nd4j.linalg.dataset.api.iterator.BaseDatasetIterator;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.fetcher.DataSetFetcher;

/**
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/26.
 */
public class VideoDataIterator extends BaseDatasetIterator {
    public VideoDataIterator(int batch, int numExamples, DataSetFetcher fetcher) {
        super(batch, numExamples, fetcher);
    }
}
