package org.niitrecvid.util;

import lombok.NonNull;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.bytedeco.javacpp.opencv_core.Size;
import org.bytedeco.javacpp.opencv_imgproc;
import org.bytedeco.javacpp.opencv_videoio.VideoCapture;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/25.
 */
public class VideoPreprocessor {
    private static final Logger log = LoggerFactory.getLogger(VideoPreprocessor.class);
    protected int height, width, numFrames;

    public VideoPreprocessor(int height, int width, int N) {
        this.height = height;
        this.width= width;
        this.numFrames = N;
    }

    /**
     * Convert a Mat to a tensor
     *
     * @param input
     * @return
     */
    public INDArray matToINDArray(Mat input) {
        DoubleBuffer data = input.data().asBuffer().asDoubleBuffer();
        double[] d = new double[data.remaining()];
        data.get(d);
        INDArray output = Nd4j.create(d, new int[]{3, input.rows(), input.cols()});
        return output;
    }

    /**
     * Load and resize + skip frames from a video
     *
     * @param filename
     * @return A tensor with size (numFrames x 3 x width x height)
     * @throws IOException
     */
    public INDArray loadVideo(@NonNull String filename) throws IOException {
        List<INDArray> vs = new ArrayList<>();
        VideoCapture videoCapture = new VideoCapture(filename);
        if (!videoCapture.isOpened()) {
            return Nd4j.zeros(1);
        }

        Mat frame = new Mat();
        Mat tmp =  new Mat();

        int N = 0;
        while (true) {
            videoCapture.shiftRight(frame);
            N++;
            if (frame.empty()) {
                break; // finish here
            }
            opencv_imgproc.resize(frame, tmp, new Size(this.width, this.height)); // resize to new size
            vs.add(matToINDArray(tmp));
        }

        videoCapture.close();
        frame.release();
        tmp.release();

        INDArray v = Nd4j.zeros(numFrames,3,this.width,this.height);
        if (N < numFrames || numFrames <= 0) {
            throw new IllegalStateException("Not enough frame to process");
        }

        int k = N / numFrames;
        int r = N % k, i, j = r;

        // Fetch frames and crop them
        for (i = 0; i < numFrames; i++) {
            v.putRow(i,vs.get(j));
            j += k;
        }

        vs.clear();
        return v;
    }
}
