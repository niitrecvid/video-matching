n04209239 shower curtain
n02165456 ladybug, ladybeetle, lady beetle, ladybird, ladybird beetle
n02966193 carousel, carrousel, merry-go-round, roundabout, whirligig
n04376876 syringe
n01943899 conch
n03794056 mousetrap
n02808440 bathtub, bathing tub, bath, tub
n03662601 lifeboat
n03888605 parallel bars, bars
n02206856 bee
n03788365 mosquito net
n02782093 balloon
n02807133 bathing cap, swimming cap
n03529860 home theater, home theatre
n02948072 candle, taper, wax light
n04579432 whistle
n01770393 scorpion
n09229709 bubble
n03676483 lipstick, lip rouge
n03443371 goblet
n03724870 mask
n04493381 tub, vat
n03729826 matchstick
n03188531 diaper, nappy, napkin
n03126707 crane
n02892767 brassiere, bra, bandeau
n03372029 flute, transverse flute
n04346328 stupa, tope
n04418357 theater curtain, theatre curtain
n04266014 space shuttle
n03240683 drilling platform, offshore rig
n03345487 fire engine, fire truck
n03888257 parachute, chute
n03937543 pill bottle
n04296562 stage
n01773549 barn spider, Araneus cavaticus
n03535780 horizontal bar, high bar
n02788148 bannister, banister, balustrade, balusters, handrail
n01630670 common newt, Triturus vulgaris
n01632777 axolotl, mud puppy, Ambystoma mexicanum
n03452741 grand piano, grand
n04116512 rubber eraser, rubber, pencil eraser
n03637318 lampshade, lamp shade
n03424325 gasmask, respirator, gas helmet
n02917067 bullet train, bullet
n02879718 bow
n04522168 vase
n07768694 pomegranate
n04590129 window shade
n04584207 wig
n04118776 rule, ruler
n04456115 torch
n01443537 goldfish, Carassius auratus
n03095699 container ship, containership, container vessel
n01833805 hummingbird
n04209133 shower cap
n02790996 barbell
n03673027 liner, ocean liner
n03388549 four-poster
n02219486 ant, emmet, pismire
n07614500 ice cream, icecream
n02799071 baseball
n03325584 feather boa, boa
n03868863 oxygen mask
n03476684 hair slide
n03721384 marimba, xylophone
n02860847 bobsled, bobsleigh, bob
n03127747 crash helmet
n03942813 ping-pong ball
n04536866 violin, fiddle
n03532672 hook, claw
n02666196 abacus
n04589890 window screen
n04462240 toyshop
n02843684 birdhouse
n04380533 table lamp
n04509417 unicycle, monocycle
n07615774 ice lolly, lolly, lollipop, popsicle
n02786058 Band Aid
n02802426 basketball
n01773797 garden spider, Aranea diademata
n04357314 sunscreen, sunblock, sun blocker
n03840681 ocarina, sweet potato
n02233338 cockroach, roach
n04404412 television, television system
n01532829 house finch, linnet, Carpodacus mexicanus
n03344393 fireboat
n02643566 lionfish
n04507155 umbrella
n03272010 electric guitar
n03388043 fountain
n03825788 nipple
n01776313 tick
n03916031 perfume, essence
n03944341 pinwheel
n02869837 bonnet, poke bonnet
n04070727 refrigerator, icebox
n02342885 hamster
n04442312 toaster
n03976657 pole
n02777292 balance beam, beam
n04251144 snorkel
n02815834 beaker
n04540053 volleyball
n03908714 pencil sharpener
n02098286 West Highland white terrier
n03935335 piggy bank, penny bank
