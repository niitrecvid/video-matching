n02883205 bow tie, bow-tie, bowtie
n04355933 sunglass
n02701002 ambulance
n04443257 tobacco shop, tobacconist shop, tobacconist
n03345487 fire engine, fire truck
n03461385 grocery store, grocery, food market, market
n02930766 cab, hack, taxi, taxicab
n04376876 syringe
n04259630 sombrero
n03970156 plunger, plumber's helper
n02823750 beer glass
n04462240 toyshop
n04204238 shopping basket
n04311174 steel drum
n04037443 racer, race car, racing car
n04525305 vending machine
n04200800 shoe shop, shoe-shop, shoe store
n04204347 shopping cart
n02927161 butcher shop, meat market
n04356056 sunglasses, dark glasses, shades
n03958227 plastic bag
n06596364 comic book
n03617480 kimono
n02807133 bathing cap, swimming cap
n03733805 measuring cup
n03877472 pajama, pyjama, pj's, jammies
n03041632 cleaver, meat cleaver, chopper
n01983481 American lobster, Northern lobster, Maine lobster, Homarus americanus
n02786058 Band Aid
n07565083 menu
n02871525 bookshop, bookstore, bookstall
n01981276 king crab, Alaska crab, Alaskan king crab, Alaska king crab, Paralithodes camtschatica
n03089624 confectionery, confectionary, candy store
n04149813 scoreboard
n03630383 lab coat, laboratory coat
n04070727 refrigerator, icebox
n03871628 packet
n02860847 bobsled, bobsleigh, bob
n04243546 slot, one-armed bandit
