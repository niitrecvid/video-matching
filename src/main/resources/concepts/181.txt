n10565667 scuba diver
n02643566 lionfish
n02219486 ant, emmet, pismire
n04507155 umbrella
n04286575 spotlight, spot
n04376876 syringe
n04152593 screen, CRT screen
n02951585 can opener, tin opener
n04296562 stage
n02965783 car mirror
n04141076 sax, saxophone
n03759954 microphone, mike
n07892512 red wine
n01494475 hammerhead, hammerhead shark
n04009552 projector
n01930112 nematode, nematode worm, roundworm
n01873310 platypus, duckbill, duckbilled platypus, duck-billed platypus, Ornithorhynchus anatinus
n03532672 hook, claw
n01770393 scorpion
n03691459 loudspeaker, speaker, speaker unit, loudspeaker system, speaker system
n03786901 mortar
n03637318 lampshade, lamp shade
n03424325 gasmask, respirator, gas helmet
n04356056 sunglasses, dark glasses, shades
n03085013 computer keyboard, keypad
n04086273 revolver, six-gun, six-shooter
n02777292 balance beam, beam
n02174001 rhinoceros beetle
n04485082 tripod
n03109150 corkscrew, bottle screw
n04069434 reflex camera
n01990800 isopod
n03832673 notebook, notebook computer
n03782006 monitor
n09229709 bubble
n04023962 punching bag, punch bag, punching ball, punchball
n03529860 home theater, home theatre
n03032252 cinema, movie theater, movie theatre, movie house, picture palace
n03916031 perfume, essence
n03180011 desktop computer
n01910747 jellyfish
n03729826 matchstick
n04404412 television, television system
n06359193 web site, website, internet site, site
