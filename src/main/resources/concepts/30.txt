n02883205 bow tie, bow-tie, bowtie
n04355933 sunglass
n04228054 ski
n04591713 wine bottle
n04418357 theater curtain, theatre curtain
n03967562 plow, plough
n04311174 steel drum
n02443484 black-footed ferret, ferret, Mustela nigripes
n03888257 parachute, chute
n03803284 muzzle
n02088632 bluetick
n03218198 dogsled, dog sled, dog sleigh
n09835506 ballplayer, baseball player
n02669723 academic gown, academic robe, judge's robe
n02892767 brassiere, bra, bandeau
n03594734 jean, blue jean, denim
n03379051 football helmet
n03388043 fountain
n02966687 carpenter's kit, tool kit
n02085620 Chihuahua
n02099712 Labrador retriever
n04209133 shower cap
n02441942 weasel
n02415577 bighorn, bighorn sheep, cimarron, Rocky Mountain bighorn, Rocky Mountain sheep, Ovis canadensis
n03544143 hourglass
n02089973 English foxhound
n03476684 hair slide
n04548362 wallet, billfold, notecase, pocketbook
n03291819 envelope
n07892512 red wine
n02492035 capuchin, ringtail, Cebus capucinus
n02108551 Tibetan mastiff
n03535780 horizontal bar, high bar
n04019541 puck, hockey puck
n04120489 running shoe
n02112706 Brabancon griffon
n02860847 bobsled, bobsleigh, bob
n02096585 Boston bull, Boston terrier
n03930313 picket fence, paling
n04200800 shoe shop, shoe-shop, shoe store
n01873310 platypus, duckbill, duckbilled platypus, duck-billed platypus, Ornithorhynchus anatinus
n03196217 digital clock
n04371430 swimming trunks, bathing trunks
n04476259 tray
n02447366 badger
n04356056 sunglasses, dark glasses, shades
n03958227 plastic bag
n02107574 Greater Swiss Mountain dog
n04522168 vase
n03888605 parallel bars, bars
n02807133 bathing cap, swimming cap
n02777292 balance beam, beam
n06359193 web site, website, internet site, site
n02093256 Staffordshire bullterrier, Staffordshire bull terrier
n02088238 basset, basset hound
n02927161 butcher shop, meat market
n04550184 wardrobe, closet, press
n03041632 cleaver, meat cleaver, chopper
n02840245 binder, ring-binder
n03908618 pencil box, pencil case
n02667093 abaya
n01990800 isopod
n07248320 book jacket, dust cover, dust jacket, dust wrapper
n01704323 triceratops
n04399382 teddy, teddy bear
n02443114 polecat, fitch, foulmart, foumart, Mustela putorius
n03782006 monitor
n09229709 bubble
n03710637 maillot
n04336792 stretcher
n03630383 lab coat, laboratory coat
n03633091 ladle
n03916031 perfume, essence
n02395406 hog, pig, grunter, squealer, Sus scrofa
n03868863 oxygen mask
n03443371 goblet
n03871628 packet
n04152593 screen, CRT screen
n03623198 knee pad
n04404412 television, television system
n03787032 mortarboard
n02110958 pug, pug-dog
