import argparse

from evaluation import *
from models import *

from include.data import *


def test_with_gt(video_model, X_test, Y_test):
    output = video_model.predict(X_test)
    print("Accuracy:")
    print("@1 = " + str(accuracy(output, Y_test, 1)))
    print("@5 = " + str(accuracy(output, Y_test, 5)))
    print("@10 = " + str(accuracy(output, Y_test, 10)))
    print("MID = " + str(mid(output,Y_test)))


def test_without_gt(video_model, X_test, Y_test):
    output = video_model.predict(X_test)
    # now matching with Y_test
    n = len(X_test)
    ranks = []
    for i in range(0, n):
        out = output[i]
        ranks.append(rank(out, Y_test))
    np.save(open('ranking.result', 'w'), ranks)


def load_test_data(h5_file):
    with h5py.File(h5_file, 'r') as f:
        X_test = f.get('X_test')[:]
        Y_test = f.get('Y_test')[:]
    return X_test, Y_test

if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description="Testing a matcher")
    argparser.add_argument("data", type=str, help="hdf5 data file")
    argparser.add_argument("video_model", type=str, help="Output video model file")
    argparser.add_argument("language_model", type=str, help="Output doc2vec model file")
    argparser.add_argument("has_gt", type=int, help="0: no gt; otherwise: has gt")
    args = argparser.parse_args()

    # load data
    X_test, Y_test = load_test_data(args.data)
    d2v = doc2vec.Doc2Vec.load(args.language_model)
    vm = VideoTextModel(300)
    vm.load_weights(args.video_model)
    # testing
    if args.has_gt == 0:
        test_without_gt(vm, X_test, Y_test)
    else:
        test_with_gt(vm, X_test, Y_test)
