import time

from evaluation import *
from gensim.utils import *
from keras.optimizers import *
from models import *

from concept_extractor import *
from include.data import *


def test(video_model, X_test, Y_test):
    output = video_model.predict(X_test)
    # now matching with Y_test
    n = len(X_test)
    ranks = []
    for i in range(0, n):
        out = output[i]
        ranks.append(rank(out, Y_test))
    np.save(open('ranking.result', 'r'), ranks)


def load_data(args, doc2vec_model):
    # load video data and captions
    files = get_files(args.train_video_dir)
    n = len(files)
    ids = list(range(0, len(files)))
    random.shuffle(ids)  # first 160 videos are used for training
    train_files = []
    val_files = []
    for i in range(0, args.num_train):
        train_files.append(files[ids[i]])
    for i in range(args.num_train, n):
        val_files.append(files[ids[i]])
    train_v = load_all_data(train_files, 5, 128)
    val_v = load_all_data(val_files, 5, 128)
    # load captions
    captions = load_v2t_captions(args.captions)

    # compute caption vectors
    caption_vs = []
    for caption in captions:
        caption_words = list(tokenize(caption, lowercase=True, deacc=True))
        caption_vec = doc2vec_model.infer_vector(caption_words)
        caption_vs.append(caption_vec)
    train_caption_v = []
    val_caption_v = []
    for i in range(0, args.num_train):
        train_caption_v.append(caption_vs[ids[i]])
    for i in range(args.num_train, n):
        val_caption_v.append(caption_vs[ids[i]])

    # load test data: do not need shuffling here
    test_files = get_files(args.test_video_dir)
    n = len(test_files)
    test_v = load_all_data(test_files, 5, 128)
    test_captions = load_v2t_captions(args.test_captions)
    test_caption_v = []
    for caption in test_captions:
        caption_words = list(tokenize(caption, lowercase=True, deacc=True))
        caption_vec = doc2vec_model.infer_vector(caption_words)
        test_caption_v.append(caption_vec)

    return train_v, train_caption_v, val_v, val_caption_v, test_v, test_caption_v, files


def train_doc2vec(args):
    # build models
    sentences = doc2vec.TaggedLineDocument(args.sentences)
    d2v_model = doc2vec_model(sentences, 10, args.v_dim)
    d2v_model.save(args.output_d2v_model)
    return d2v_model


def train(args, video_model, X_train, Y_train, X_val, Y_val):
    # configuration
    sgd = SGD(lr=args.lr, decay=1e-6, momentum=0.9, nesterov=True)
    video_model.compile(loss='mean_squared_error', optimizer=sgd)

    # now fitting
    video_model.fit(X_train, Y_train,
                    batch_size=args.batch_size,
                    nb_epoch=args.epochs,
                    verbose=1,
                    validation_data=(X_val, Y_val),
                    shuffle=True)
    return video_model


def save_split(X_train, Y_train, X_val, Y_val, X_test, Y_test, filename):
    with h5py.File(filename, 'w') as f:
        f['X_train'] = X_train
        f['X_val'] = X_val
        f['X_test'] = X_test
        f['Y_train'] = Y_train
        f['Y_val'] = Y_val
        f['Y_test'] = Y_test


def load_split(filename):
    with h5py.File(filename, 'r') as f:
        X_train = f.get('X_train')[:]
        Y_train = f.get('Y_train')[:]
        X_val = f.get('X_val')[:]
        Y_val = f.get('Y_val')[:]
        X_test = f.get('X_test')[:]
        Y_test = f.get('Y_test')[:]
    return X_train, Y_train, X_val, Y_val, X_test, Y_test


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description="Training a matcher")
    argparser.add_argument("sentences", type=str, help="sentence file")
    argparser.add_argument("train_video_dir", type=str, help="videos for train/val")
    argparser.add_argument("test_video_dir", type=str, help="videos for test")
    argparser.add_argument("captions", type=str, help="captions file")
    argparser.add_argument("test_captions", type=str, help="captions file for test")
    argparser.add_argument("lr", type=float, help="learning rate")
    argparser.add_argument("num_train", type=int, help="train size")
    argparser.add_argument("batch_size", type=int, help="batch size")
    argparser.add_argument("epochs", type=int, help="number of epochs")
    argparser.add_argument("v_dim", type=int, help="vector space dimensionality")
    argparser.add_argument("split_file", type=str, help="Output split file")
    argparser.add_argument("output_video_model", type=str, help="Output video model file")
    argparser.add_argument("output_d2v_model", type=str, help="Output doc2vec model file")
    argparser.add_argument("finetune_from", type=str, help="finetune from a model")
    args = argparser.parse_args()

    # Loading doc2vec model
    if isfile(args.output_d2v_model):
        d2v = doc2vec.Doc2Vec.load(args.output_d2v_model)
    else:
        d2v = train_doc2vec(args)

    # Loading split
    if isfile(args.split_file):
        X_train, Y_train, X_val, Y_val, X_test, Y_test = load_split(args.split_file)
    else:
        train_v, train_caption_v, val_v, val_caption_v, test_v, test_caption_v, files = load_data(args, d2v)
        X_train = np.asarray(train_v)
        Y_train = np.asarray(train_caption_v)
        X_val = np.asarray(val_v)
        Y_val = np.asarray(val_caption_v)
        X_test = np.asarray(test_v)
        Y_test = np.asarray(test_caption_v)
        save_split(X_train, Y_train, X_val, Y_val, X_test, Y_test, args.split_file)

    # Training (and Validating)
    print('============================== TRAINING ==============================')
    video_model = VideoTextModel(args.v_dim)
    if args.finetune_from != "no_finetune":
        video_model.load_weights(args.finetune_from)

    video_model.summary()
    # visualize
    # SVG(model_to_dot(video_model).create(prog='dot', format='svg'))
    # plot(video_model,'model.png')
    if args.epochs > 0:
        train(args, video_model, X_train, Y_train, X_val, Y_val)
        video_model.save_weights(args.output_video_model + str(int(round(time.time() * 1000))))
    print('======================== FINISHED TRAINING ===========================')

    # Testing
    print('============================== TESTING ===============================')
    test(video_model, X_test, Y_test)
    print('======================== FINISHED TESTING ============================')





