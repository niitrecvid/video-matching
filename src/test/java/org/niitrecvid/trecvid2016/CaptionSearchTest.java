package org.niitrecvid.trecvid2016;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/23.
 */
public class CaptionSearchTest {
    private static final Logger log = LoggerFactory.getLogger(CaptionSearchTest.class);
    CaptionSearch searcher;
    String conceptFolder;
    int nVideos = 20;

    @Before
    public void setUp() throws Exception {
        SimpleMatcher matcher = new SimpleMatcher();
        matcher.loadCaptions(System.getProperty("user.dir") + File.separator + "src/main/resources/tvv2t/vines.textDescription.A.trainingSet");
        searcher =  new CaptionSearch(matcher);
        conceptFolder = System.getProperty("user.dir") + File.separator + "src/main/resources/concepts" + File.separator;
    }

    @Test
    public void search() throws Exception {
        double mir = 0.0;
        double p1 = 0, p5 = 0, p10 = 0;
        for (int i = 0; i < nVideos; i++) {
            String conceptFile = conceptFolder + (i+1) + ".txt";
            searcher.search(conceptFile);
            mir += searcher.invertedRank(i);
            p1 += searcher.precision(i,1);
            p5 += searcher.precision(i,5);
            p10 += searcher.precision(i,10);
        }

        log.info("MIR = " + (mir/nVideos));
        log.info("P@1 = " + (p1/nVideos));
        log.info("P@5 = " + (p5/nVideos));
        log.info("P@10 = " + (p10/nVideos));
    }

}
