package org.niitrecvid.trecvid2016;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/22.
 */
public class SimpleMatcherTest {
    private static final Logger log = LoggerFactory.getLogger(SimpleMatcherTest.class);
    SimpleMatcher matcher;
    String captionFile;
    String conceptFile;

    @Before
    public void setUp() throws Exception {
        matcher = new SimpleMatcher();
        captionFile = System.getProperty("user.dir") + File.separator + "src/main/resources/tvv2t/vines.textDescription.A.trainingSet";
        conceptFile = System.getProperty("user.dir") + File.separator + "src/main/resources/concepts/2.txt";
    }

    @Test
    public void loadConcepts() throws Exception {
        Set<String> concepts = SimpleMatcher.loadConcepts(conceptFile,17);
        log.info("Number of concepts in first video: " + concepts.size());
        log.info("All concepts:" + concepts.toString());
    }

    @Test
    public void loadCaptions() throws Exception {
        matcher.loadCaptions(captionFile);
    }

    @Test
    public void query() throws Exception {
        Set<String> concepts = SimpleMatcher.loadConcepts(conceptFile,  0);
        List<String> refinedConcepts = new ArrayList<>();
        matcher.loadCaptions(captionFile);
        IndexReader indexReader = DirectoryReader.open(matcher.getIndex());
        IndexSearcher searcher = new IndexSearcher(indexReader);
        final double[] captionScores = new double[matcher.numDocs()];
        Integer[] idx = new Integer[captionScores.length];
        for (int i = 0; i < captionScores.length; i++) {
            captionScores[i] = 0.0f;
            idx[i] = i;
        }
        for (String concept:concepts) {
            ScoreDoc[] docs = matcher.query(concept, 10);
            log.info("========== Concept: " + concept+ " =============");
            if (docs.length < 1) {
                log.info("0 candidates --> bad concept");
            } else {
                log.info("good concept");
                refinedConcepts.add(concept);
                for (int i = 0; i < docs.length; i++) {
                    Document d = searcher.doc(docs[i].doc);
                    log.info("ID: " + docs[i].doc + "; caption: " + d.get("caption"));
                    log.info("Score: " + docs[i].score);
                    captionScores[docs[i].doc] += docs[i].score;
                }
            }
        }

        log.info("Good concepts for this video: " + refinedConcepts.toString());

        Arrays.sort(idx, new Comparator<Integer>() {
            @Override public int compare(final Integer o1, final Integer o2) {
                return Double.compare(captionScores[o2], captionScores[o1]);
            }
        });
        log.info("Top 10 captions:");
        for (int i = 0; i < 10; i++) {
            Document d = searcher.doc(idx[i]);
            log.info("Score: " + captionScores[idx[i]] + "; " + d.get("caption"));
        }

        indexReader.close();
    }

    @Test
    public void finish() throws Exception {

    }

}
