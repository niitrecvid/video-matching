package org.niitrecvid.util;

import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.api.split.InputSplit;
import org.junit.Before;
import org.junit.Test;
import org.nd4j.linalg.dataset.DataSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/26.
 */
public class VideoDataFetcherTest {
    private static final Logger log = LoggerFactory.getLogger(VideoDataFetcherTest.class);
    VideoDataFetcher fetcher;

    @Before
    public void setUp() throws Exception {
        Random random = new Random(12345);
        String rootDir = System.getProperty("user.dir") + File.separator + "src/main/resources/tvv2t/videos/train/";
        FileSplit fileSplit = new FileSplit(new File(rootDir), random);
        ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();
        BalancedPathFilter pathFilter = new BalancedPathFilter(random, new String[] {"mp4"}, labelMaker);
        InputSplit[] splits = fileSplit.sample(pathFilter, 80, 20);
        List<String> captions = new ArrayList<>();
        for (int i = 0; i < splits[0].length(); i++) {
            captions.add("I am Bob"); // dummy captions
        }
        fetcher = new VideoDataFetcher(splits[0].locations(), captions, 128, 128, 20);
    }

    @Test
    public void fetch() throws Exception {
        // Note: we have only 200 videos: 160 for training and 40 for testing
        fetcher.fetch(128);
        DataSet ds = fetcher.next();
        assertEquals(128, ds.numExamples());
        fetcher.fetch(128);
        ds = fetcher.next();
        assertEquals(32, ds.numExamples());
    }

}
