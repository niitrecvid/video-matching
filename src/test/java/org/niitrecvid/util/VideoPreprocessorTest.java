package org.niitrecvid.util;

import org.junit.Before;
import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import static org.junit.Assert.*;

/**
 * Created by Tuan Nguyen <tuannguyen.research@gmail.com> on 2016/09/26.
 */
public class VideoPreprocessorTest {
    private static final Logger log = LoggerFactory.getLogger(VideoPreprocessorTest.class);
    VideoPreprocessor preprocessor;
    String filename;

    @Before
    public void setUp() throws Exception {
        preprocessor = new VideoPreprocessor(128,128,20);
        filename = System.getProperty("user.dir") + File.separator + "src/main/resources/tvv2t/videos/train/1.mp4";
    }

    @Test
    public void matToINDArray() throws Exception {

    }

    @Test
    public void loadVideo() throws Exception {
        INDArray v = preprocessor.loadVideo(filename);
        assertArrayEquals(new int[] {20,3,128,128}, v.shape());
    }

}
